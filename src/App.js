import React from 'react';
import './App.css';

import { Layout, Menu, Icon, Badge, Typography } from 'antd';
import { PageHeader, Tag, Button, Dropdown, Statistic, Descriptions, Row, Card, Divider, Tabs, Comment, Avatar, Drawer, Popover } from 'antd';

const { Header, Content, Footer, Sider } = Layout;
const { TabPane } = Tabs;
const { SubMenu } = Menu;
const { Text } = Typography;


const menu = (
  <Menu>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="http://www.test.com/">
        Share
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="http://www.test.com/">
        Delete
      </a>
    </Menu.Item>
    <Menu.Item>
      <a target="_blank" rel="noopener noreferrer" href="http://www.test.com/">
        3rd menu item
      </a>
    </Menu.Item>
  </Menu>
);

const apps = (
  <div>
    {/* <div style={{ display: 'inline-block', margin: '10px' }}>
      <Button type="dashed" shape="circle" icon="appstore" size="large"/>
      <h4>Issues</h4>
    </div>
    <div style={{ display: 'inline-block', margin: '10px' }}>
      <Button type="dashed" shape="circle" icon="appstore" size="large"/>
      <h4>Git</h4>
    </div>
    <div style={{ display: 'inline-block', margin: '10px' }}>
      <Button type="dashed" shape="circle" icon="appstore" size="large"/>
      <h4>Docs</h4>
    </div>
    <br />
    <div style={{ display: 'inline-block', margin: '10px' }}>
      <Button type="dashed" shape="circle" icon="appstore" size="large"/>
      <h4>Automations</h4>
    </div>
    <div style={{ display: 'inline-block', margin: '10px' }}>
      <Button type="dashed" shape="circle" icon="appstore" size="large"/>
      <h4>Artifacts</h4>
    </div> */}
    <Menu
          //style={{ width: 256 }}
          defaultSelectedKeys={['1']}
          mode='inline'
          theme='light'
        >
          <Menu.Item key="1">
            <Icon type="project" />
            Issues
          </Menu.Item>
          <Menu.Item key="2">
            <Icon type="snippets" />
            Documentation
          </Menu.Item>
          <Menu.Item key="3">
            <Icon type="branches" />
            Source Control
          </Menu.Item>
          <Menu.Item key="4">
            <Icon type="code" />
            Automations
          </Menu.Item>
          <Menu.Item key="5">
            <Icon type="build" />
            Artifacts
          </Menu.Item>
          <Menu.Item key="6">
            <Icon type="safety" />
            Security
          </Menu.Item>
          <Divider style={{ margin: '3px 0' }} />
          <Menu.Item key="7">
            <Icon type="setting" />
            Organization
          </Menu.Item>
    </Menu>
  </div>
);

const ExampleComment = ({ children }) => (
  <Comment
    actions={[<span key="comment-nested-reply-to">Reply to</span>]}
    author={<a>Dexter Claydon</a>}
    avatar={
      <Avatar
        src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
        alt="Han Solo"
      />
    }
    content={
      <p>
        We supply a series of design principles, practical patterns and high quality design resources. Should be interactive.
      </p>
    }
  >
    {children}
  </Comment>
);

const DropdownMenu = () => {
  return (
    <Dropdown key="more" overlay={menu}>
      <Button
        style={{
          border: 'none',
          padding: 0,
        }}
      >
        <Icon
          type="ellipsis"
          style={{
            fontSize: 20,
            verticalAlign: 'top',
          }}
        />
      </Button>
    </Dropdown>
  );
};

const routes = [
  {
    path: 'index',
    breadcrumbName: 'First-level Menu',
  },
  {
    path: 'first',
    breadcrumbName: 'Second-level Menu',
  },
  {
    path: 'second',
    breadcrumbName: 'Third-level Menu',
  },
];

function App() {
  return (
    <div id="app">
      <Layout
        style={{
          height: '100%',
          background: 'white'
        }}
      >
        <Sider
          width='240'
          theme='light'
          style={{
            borderRight: '1px solid rgb(235, 237, 240)',
            background: 'rgb(251, 251, 251)',
          }}
        >
          <div class="top-banner" style={{ padding: '14px', height: '67px', borderBottom: '1px solid rgb(235, 237, 240)', }}>
            {/* <Avatar style={{ backgroundColor: "#4a78e8" }} size="large">
              &nbsp;
            </Avatar>
            <div 
              style={{
                display: 'inline',
                paddingLeft: '8px',
                fontSize: '22px',
                lineHeight: '18px',
                fontWeight: 200
              }}
            >
              Indent.Cloud
            </div> */}
            {/* <div class="logo">
              <div class="placeholder-logo">&nbsp;</div>
              <div class="logo-text">
                <span class="indent">Indent</span><span class="dot">.</span><span class="cloud">Cloud</span>
              </div>
            </div> */}
            <div class="placeholder-logo">&nbsp;</div>
            
            <Popover content={apps} title="Apps" trigger="click">
              <Button shape="circle" icon="appstore" size="large" style={{ float: 'right' }}/>
            </Popover>
          </div>

          <div class="links">
            <Menu
              style={{ background: 'none', border: 'none' }}
              mode='inline'
              theme='light'
            >
              <Menu.Item key="1">
                <Icon type="search" />
                Search
              </Menu.Item>
              <Menu.Item key="2">
                <Icon type="inbox" />
                Inbox
              </Menu.Item>
              <Menu.Item key="3">
                <Icon type="plus" />
                Create Issue
              </Menu.Item>
            </Menu>

            <Divider style={{ margin: '3px 0' }} />

            <Menu
              style={{ background: 'none', border: 'none' }}
              mode='inline'
              theme='light'
            >
                <Menu.ItemGroup key="g1" title="Indent UI">
                  <Menu.Item key="1">
                    <Icon type="compass" />
                    Issues
                  </Menu.Item>
                  <Menu.Item key="2">
                    <Icon type="profile" />
                    Boards
                  </Menu.Item>
                  <Menu.Item key="3">
                    <Icon type="carry-out" />
                    Releases
                  </Menu.Item>
                  <Menu.Item key="4">
                    <Icon type="rise" />
                    Reports
                  </Menu.Item>
                  <Menu.Item key="5">
                    <Icon type="tags" />
                    Tags
                  </Menu.Item>
                </Menu.ItemGroup>
                <Menu.ItemGroup key="g2" title="Projects">
                  <Menu.Item key="1">
                    <Icon type="right" />
                    Infrastructure  <Text type="secondary" style={{ fontSize: '12px' }}>(INFRA)</Text>
                  </Menu.Item>
                  <Menu.Item key="2">
                    <Icon type="right" />
                    iOS App <Text type="secondary" style={{ fontSize: '12px' }}>(IOS)</Text>
                  </Menu.Item>
                  <Menu.Item key="3">
                    <Icon type="right" />
                    Android App <Text type="secondary" style={{ fontSize: '12px' }}>(ANDROID)</Text>
                  </Menu.Item>
                  <Menu.Item key="1">
                    <Icon type="double-right" />
                    Browse All
                  </Menu.Item>
                </Menu.ItemGroup>
            </Menu>
          </div>
        </Sider>
        <Layout style={{ background: 'white' }}>
          <Header
            style={{
              background: 'white',
              height: '65.6px',
              padding: '0',
              position: 'fixed', zIndex: 1, width: 'calc(100% - 290px - 240px)'
            }}
          >
            <PageHeader
              onBack={() => null}
              title="Create UI of the issue view page"
              subTitle="INDENT-1856"
              // subTitle="indent/web#1856"
              tags={<Tag color="blue">In Progress</Tag>}
              //avatar={{ src: 'https://avatars1.githubusercontent.com/u/8186664?s=460&v=4' }}
              // breadcrumb={{ routes }}
              extra={[
                <Button icon="edit" />,
                <DropdownMenu key="more" />,
              ]}
              style={{
                borderBottom: '1px solid rgb(235, 237, 240)',
              }}
            />
          </Header>
          <Content
            style={{
              padding: '15px',
              background: 'white',
              marginTop: '65.5px'
            }}
          >
            <p>
              <p>This is the ticket description</p>
              <p>It should contain the content and meaning of the ticket and provide all the information you could possibly need</p>
              <p>Card content</p>
            </p>
 
            <Card size="small" title="Linked Issues">
              <p>is <strong>blocked by</strong> <a>INDENT-1292</a></p>
            </Card>

            <Tabs defaultActiveKey="1">
              <TabPane tab="Comments" key="1">
                <ExampleComment>
                  <ExampleComment>
                    <ExampleComment />
                    <ExampleComment />
                  </ExampleComment>
                </ExampleComment>
                <ExampleComment />
                <ExampleComment />
              </TabPane>
              {/* <TabPane tab="Activity" key="2">
                Content of Tab Pane 2
              </TabPane>
              <TabPane tab="History" key="3">
                Content of Tab Pane 3
              </TabPane> */}
            </Tabs>
          </Content>
          {/* <Footer>Footer</Footer> */}
        </Layout>
        <Sider
          width='290'
          theme='light'
          style={{
            borderLeft: '1px solid rgb(235, 237, 240)',
            background: 'rgb(251, 251, 251)',
            padding: '10px',
            color: 'black',
            fontWeight: 400
          }}
        >
          <p>
            <h3 style={{ fontSize: '15px', textTransform: 'uppercase', color: 'rgb(94, 108, 132)' }}>Reporter</h3>
            <Avatar style={{ marginRight: '3px' }}>AC</Avatar> Aaron Claydon
          </p>

          <p>
            <h3 style={{ fontSize: '15px', textTransform: 'uppercase', color: 'rgb(94, 108, 132)' }}>Assigned</h3>
            <Avatar style={{ marginRight: '3px' }}>R</Avatar> Robert McDavidson
          </p>
          
          <p>
            <h3 style={{ fontSize: '15px', textTransform: 'uppercase', color: 'rgb(94, 108, 132)' }}>Priority</h3>
            Medium
          </p>

          <p>
            <h3 style={{ fontSize: '15px', textTransform: 'uppercase', color: 'rgb(94, 108, 132)' }}>Epic</h3>
            <Tag color="blue">Create a PoC of the issues platform</Tag>
          </p>

          <p>
            <h3 style={{ fontSize: '15px', textTransform: 'uppercase', color: 'rgb(94, 108, 132)' }}>Tags</h3>
            <div style={{ lineHeight: '30px' }}>
              <Tag color="volcano">Issues</Tag>
              <Tag color="green">PoC</Tag>
              <Tag color="purple">UI</Tag>
              <Tag color="blue">Design</Tag>
              <Tag color="magenta">1st Pass</Tag>
              <Tag style={{ background: '#fff', borderStyle: 'dashed' }}>
                <Icon type="plus" /> New Tag
              </Tag>
            </div>
          </p>

          <p>
            <h3 style={{ fontSize: '15px', textTransform: 'uppercase', color: 'rgb(94, 108, 132)' }}>Cycle</h3>
            <a>Team Meerkat Sprint 34</a>
          </p>

          <p>
            <h3 style={{ fontSize: '15px', textTransform: 'uppercase', color: 'rgb(94, 108, 132)' }}>Development</h3>
            <a>12 commits</a><br />
            <a>2 branch</a><br />
            <a>2 reviews</a><br />
            <a>12 builds</a><br />
          </p>
        </Sider>
      </Layout>
    </div>
  );
}

export default App;
